package com.springsource.roo.fitnesswebsite.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class FitnessPlanner {

    /**
     */
    @NotNull
    @Size(min = 2)
    private String Gender;

    /**
     */
    private Float Height;

    /**
     */
    private Float Weight;

    /**
     */
    @NotNull
    @Size(min = 2)
    private String FitnessType;
}
