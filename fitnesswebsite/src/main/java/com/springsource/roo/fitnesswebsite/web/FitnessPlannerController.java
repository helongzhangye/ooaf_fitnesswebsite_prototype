package com.springsource.roo.fitnesswebsite.web;
import com.springsource.roo.fitnesswebsite.domain.FitnessPlanner;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/fitnessplanners")
@Controller
@RooWebScaffold(path = "fitnessplanners", formBackingObject = FitnessPlanner.class)
public class FitnessPlannerController {
}
