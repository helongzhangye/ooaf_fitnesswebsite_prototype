package com.springsource.roo.fitnesswebsite.web;
import com.springsource.roo.fitnesswebsite.domain.ArrangementOfPlan;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/arrangementofplans")
@Controller
@RooWebScaffold(path = "arrangementofplans", formBackingObject = ArrangementOfPlan.class)
public class ArrangementOfPlanController {
}
