package com.springsource.roo.fitnesswebsite.web;
import com.springsource.roo.fitnesswebsite.domain.DailyEvaluation;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/dailyevaluations")
@Controller
@RooWebScaffold(path = "dailyevaluations", formBackingObject = DailyEvaluation.class)
public class DailyEvaluationController {
}
