package com.springsource.roo.fitnesswebsite.web;
import com.springsource.roo.fitnesswebsite.domain.History;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/historys")
@Controller
@RooWebScaffold(path = "historys", formBackingObject = History.class)
public class HistoryController {
}
