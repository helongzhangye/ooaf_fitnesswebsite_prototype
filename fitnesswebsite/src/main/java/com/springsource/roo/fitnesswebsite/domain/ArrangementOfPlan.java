package com.springsource.roo.fitnesswebsite.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import java.util.Date;

import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class ArrangementOfPlan {

	/**
	 */

	@DateTimeFormat(style = "M-")
	private Date schedule;

	/**
	 */
	@OneToOne
	private FitnessPlanner planner;


	/**
	 */
	@NotNull
	@Size(min = 2)
	private String feedBack;
}
