package com.springsource.roo.fitnesswebsite.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import javax.validation.constraints.AssertTrue;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class DailyEvaluation {
	
	/**
     */
	@ManyToOne
    private ArrangementOfPlan WhichDay;

    /**
     */
    private Float Goal;

    /**
     */
    private Float CaloryConsumed;

    /**
     */
    private Float CaloryAbsorbed;

    /**
     */
    private Float SittingTime;

    /**
     */
    @AssertTrue
    private Boolean RegularDiet;

    /**
     */
    private Float HealthScore;

    /**
     */
    @AssertTrue
    private Boolean SelfMonitoring;


}
