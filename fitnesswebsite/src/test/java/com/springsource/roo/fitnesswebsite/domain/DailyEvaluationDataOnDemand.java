package com.springsource.roo.fitnesswebsite.domain;
import org.springframework.roo.addon.dod.RooDataOnDemand;

@RooDataOnDemand(entity = DailyEvaluation.class)
public class DailyEvaluationDataOnDemand {
}
