package com.springsource.roo.fitnesswebsite.domain;
import org.springframework.roo.addon.dod.RooDataOnDemand;

@RooDataOnDemand(entity = FitnessPlanner.class)
public class FitnessPlannerDataOnDemand {
}
